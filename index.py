from subprocess import call
from random import randint
import sys
import os
import time

while True:
    filename = 'dict'
    if not os.path.isfile(filename):
        print('File not exists')
        sys.exit(0)

    f = open(filename)
    content = f.readlines()
    i = randint(0, len(content) - 1)

    en_rus = content[i].split(' -- ')
    en = en_rus[0]
    rus = en_rus[1]

    try:
        call(["notify-send", en, rus, "-t", "5000"])
    except:
        print('Epic fail', i)

    time.sleep(50)
